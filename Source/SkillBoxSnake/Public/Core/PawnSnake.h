// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InputAction.h"
#include "PawnSnake.generated.h"


class UCameraComponent;
class ASnake;
class UInputAction;
class UInputMappingContext;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS()
class SKILLBOXSNAKE_API APawnSnake : public APawn
{
    GENERATED_BODY()



public:
    APawnSnake();

    UPROPERTY(BlueprintReadOnly)
    UCameraComponent* Camera { nullptr };

    UPROPERTY(BlueprintReadOnly)
    ASnake* Snake { nullptr };

    UPROPERTY(EditDefaultsOnly, Category = "SnakeParameters")
    TSubclassOf<ASnake> SnakeClass;

    UPROPERTY(EditAnywhere, Category = "Input")
    UInputAction* InputAction { nullptr };

    UPROPERTY(EditAnywhere, Category = "Input")
    UInputMappingContext* InputMapping { nullptr };

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeParameters")
    FVector SpawnOffset;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float CameraDistance = 1900.0f;

    UPROPERTY(BlueprintAssignable)
    FOnDead OnDeadDelegate;

protected:
    virtual void BeginPlay() override;

public:
    virtual void Tick(float DeltaTime) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    UFUNCTION(BlueprintCallable)
    void SpawnSnake();

    UFUNCTION(BlueprintCallable)
    void DestroySnake();

    UFUNCTION()
    void HandlerInputAction(const FInputActionInstance& Instance);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void Dead();
    virtual void Dead_Implementation();


    UFUNCTION()
    void HandleOnDead();
};
