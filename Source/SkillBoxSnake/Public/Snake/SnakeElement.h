// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeElement.generated.h"


class UStaticMeshComponent;
class ASnake;


DECLARE_DELEGATE(FOverlapSnakeElement);
DECLARE_DELEGATE(FOverlapFood);
DECLARE_DELEGATE(FOverlapWall);
DECLARE_DELEGATE(FOverlapWarp);

UCLASS()
class SKILLBOXSNAKE_API ASnakeElement : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	ASnakeElement();


    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    UStaticMeshComponent* MeshComponent { nullptr };

    FOverlapSnakeElement OverlapSnakeElementDelegate;

    FOverlapFood OverlapFoodDelegate;

    FOverlapWall OverlapWallDelegate;

    FOverlapWarp OverlapWarpDelegate;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

    virtual void Interact(AActor* InteractableActor) override;

    UFUNCTION()
    void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
        AActor* OtherActor,
        UPrimitiveComponent* OtherComp,
        int32 OtherBodyIndex,
        bool bFromSweep,
        const FHitResult& SweepResult);

    UFUNCTION()
    void OnOverlapedFood();

    UFUNCTION()
    void OnOverlapedWall();

    UFUNCTION()
    void OnOverlapedWarp();
};
