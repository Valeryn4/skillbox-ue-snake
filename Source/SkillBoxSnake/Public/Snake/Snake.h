// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TimerHandle.h"
#include "Snake.generated.h"


class ASnakeElement;
class AFood;


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeadSnake);

UCLASS()
class SKILLBOXSNAKE_API ASnake : public AActor
{
    GENERATED_BODY()

    FVector CurrentDirectionSnake;

public:
    ASnake();

    UPROPERTY(EditDefaultsOnly, Category = "SnakeParameters");
    FVector DirectionSnake;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeParameters")
    bool bStopped = false;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", ClampMax = "2000"), Category = "SnakeParameters")
    int ElementStartingCount = 3;

    UPROPERTY(BlueprintReadOnly)
    TArray<ASnakeElement*> ElementsList;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeParameters")
    float Speed = 100.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeParameters")
    float TickTimeSnake = 1.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SnakeParameters")
    float OffsetElement = 100.0f;

    UPROPERTY(EditDefaultsOnly, Category = "SnakeParameters")
    TSubclassOf<ASnakeElement> SnakeElementClass;

    UPROPERTY(EditDefaultsOnly, Category = "SnakeParameters")
    TSubclassOf<ASnakeElement> SnakeHeadElementClass;

    FTimerHandle TimerHandle;

    UPROPERTY(BlueprintAssignable)
    FOnDeadSnake OnDeadDelegate;

protected:
    virtual void BeginPlay() override;

    void InitElement(const FTransform& Transform, const TSubclassOf<ASnakeElement>& ClassElement);
    void InitHead();
    void InitTail();
public:

    virtual void Tick(float DeltaTime) override;

    UFUNCTION()
    void DestroySnakeElements();

    UFUNCTION()
    void Move();

    UFUNCTION()
    void AddElement();

    UFUNCTION()
    void SetNewDirection(const FVector& NewDirection);

    UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
    void Dead();
    virtual void Dead_Implementation();

};
