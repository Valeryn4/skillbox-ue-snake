// Copyright Epic Games, Inc. All Rights Reserved.

#include "SkillBoxSnake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SkillBoxSnake, "SkillBoxSnake" );
