// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake/Wall.h"
#include "Snake/SnakeElement.h"

AWall::AWall()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* InteractableActor)
{
    if (IsValid(InteractableActor))
    {
        if (ASnakeElement* SnakeElement = Cast<ASnakeElement>(InteractableActor))
        {
            SnakeElement->OnOverlapedWall();
        }
    }
}

