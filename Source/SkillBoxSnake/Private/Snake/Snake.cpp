// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake/Snake.h"
#include "Snake/SnakeElement.h"
#include "Snake/Food.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASnake::ASnake()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnake::BeginPlay()
{
    Super::BeginPlay();

    InitHead();

    InitTail();
}

void ASnake::InitElement(const FTransform& Transform, const TSubclassOf<ASnakeElement>& ClassElement)
{

    ASnakeElement* Element = GetWorld()->SpawnActor<ASnakeElement>(ClassElement, Transform);
    ElementsList.Add(Element);

}

void ASnake::InitHead()
{
    InitElement(FTransform(), SnakeHeadElementClass);

    ASnakeElement* Head = ElementsList[0];
    Head->OverlapSnakeElementDelegate.BindLambda([this]()
        {
            if (!IsValid(this))
                return;
            OnDeadDelegate.Broadcast();
            Dead();
        });

    Head->OverlapFoodDelegate.BindLambda([this]()
        {
            if (!IsValid(this))
                return;
            AddElement();
        });

    Head->OverlapWallDelegate.BindLambda([this]()
        {
            if (!IsValid(this))
                return;
            OnDeadDelegate.Broadcast();
            Dead();
        });

    Head->OverlapWarpDelegate.BindLambda([this, Head]()
        {
            if (!IsValid(this) || !IsValid(Head))
                return;

            FVector FixedDirection = DirectionSnake.GetAbs(); //(-1,0,0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: FixedDirection: %s"), *FixedDirection.ToString());

            FVector InverseFixedDirection = FVector::One() - FixedDirection; //(0, 1, 1)
            UE_LOG(LogTemp, Warning, TEXT("WARP: InverseFixedDirection: %s"), *InverseFixedDirection.ToString());

            FVector OldLocation = Head->GetActorLocation(); //(1000,1000,0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: OldLocation: %s"), *OldLocation.ToString());

            FVector FixedOldLocation = OldLocation * InverseFixedDirection; //(0,1000,0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: FixedOldLocation: %s"), *FixedOldLocation.ToString());

            FVector NewLocation = OldLocation * FixedDirection * -1.f; //(-1000, 0, 0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: NewLocation: %s"), *NewLocation.ToString());
            NewLocation += FixedOldLocation; //(-1000, 1000, 0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: NewLocation: %s"), *NewLocation.ToString());

            NewLocation += DirectionSnake * Speed; //(-900, 1000, 0)
            UE_LOG(LogTemp, Warning, TEXT("WARP: NewLocation: %s"), *NewLocation.ToString());

            Head->SetActorLocation(NewLocation);
        });
}

void ASnake::InitTail()
{
    for (int i = 0; i < ElementStartingCount; ++i)
    {
        FVector NewPosition(DirectionSnake * -1.0 * (i + 1) * OffsetElement);
        InitElement(FTransform(NewPosition) , SnakeElementClass);
    }
}


// Called every frame
void ASnake::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

void ASnake::DestroySnakeElements()
{
    for (ASnakeElement* Element : ElementsList)
    {
        if (Element)
        {
            Element->Destroy();
        }
    }

}


void ASnake::Move()
{
    if (!IsValid(this))
        return;

    CurrentDirectionSnake = DirectionSnake;
    ASnakeElement* Head = ElementsList[0];
    if (!IsValid(Head))
        return;
    Head->SetActorEnableCollision(false);

    for (int i = ElementsList.Num() - 1; i > 0; i--)
    {
        ASnakeElement* PrevElement = ElementsList[i - 1];
        ASnakeElement* Element = ElementsList[i];
        FRotator PlayerRot = UKismetMathLibrary::FindLookAtRotation(Element->GetActorLocation(), PrevElement->GetActorLocation());
        Element->SetActorLocation(PrevElement->GetActorLocation());
        Element->SetActorRotation(PlayerRot);
    }

    Head->AddActorWorldOffset(CurrentDirectionSnake * Speed);
    Head->SetActorRotation(CurrentDirectionSnake.Rotation());
    Head->SetActorEnableCollision(true);
}

void ASnake::AddElement()
{

    ASnakeElement* LastElement = ElementsList.Last();
    FVector Position = LastElement->GetActorLocation();
    FRotator Rotation = LastElement->GetActorRotation();
    FTransform Transform(Rotation, Position);

    InitElement(Transform, SnakeElementClass);
}

void ASnake::SetNewDirection(const FVector& NewDirection)
{
    if (NewDirection.Length() <= 0.01f)
    {
        return;
    }

    FVector FixedDirection = NewDirection;
    FVector Vertical = FVector(1.0, 0.0, 0.0) * NewDirection;
    if (Vertical.Length() >= 0.01f)
    {
        FixedDirection = Vertical;
    }

    FVector Horizontal = FVector(0.0, 1.0, 0.0) * NewDirection;
    if (Horizontal.Length() >= 0.01f)
    {
        FixedDirection = Horizontal;
    }

    FixedDirection.Normalize();
    if ((CurrentDirectionSnake + FixedDirection).Length() <= 0.01f)
    {
        return;
    }

    DirectionSnake = FixedDirection;

}

void ASnake::Dead_Implementation()
{
}
