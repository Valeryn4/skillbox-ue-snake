// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake/Warp.h"
#include "Snake/SnakeElement.h"

// Sets default values
AWarp::AWarp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AWarp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWarp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWarp::Interact(AActor* InteractableActor)
{
    if (IsValid(InteractableActor))
    {
        if (ASnakeElement* SnakeElement = Cast<ASnakeElement>(InteractableActor))
        {
            SnakeElement->OnOverlapedWarp();
        }
    }
}

