// Fill out your copyright notice in the Description page of Project Settings.


#include "Snake/SnakeElement.h"
#include "Snake/Snake.h"
#include "Components/MeshComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ASnakeElement::ASnakeElement()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
    RootComponent = MeshComponent;

    MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ASnakeElement::Interact(AActor* InteractableActor)
{
    if (IsValid(InteractableActor))
    {
        if (ASnakeElement * SnakeElement = Cast<ASnakeElement>(InteractableActor))
        {
            OverlapSnakeElementDelegate.ExecuteIfBound();
        }
    }
}

void ASnakeElement::OnOverlapedFood()
{
    OverlapFoodDelegate.ExecuteIfBound();
}

void ASnakeElement::OnOverlapedWall()
{
    OverlapWallDelegate.ExecuteIfBound();
}

void ASnakeElement::OnOverlapedWarp()
{
    OverlapWarpDelegate.ExecuteIfBound();
}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
    Super::BeginPlay();
    MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElement::HandleBeginOverlap);
}

void ASnakeElement::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
    AActor* OtherActor,
    UPrimitiveComponent* OtherComp,
    int32 OtherBodyIndex,
    bool bFromSweep,
    const FHitResult& SweepResult)
{
    if (IsValid(OtherActor))
    {
        if (IInteractable* InteractableObject = Cast<IInteractable>(OtherActor))
        {
            InteractableObject->Interact(this);
        }
    }
}

// Called every frame
void ASnakeElement::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

