// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/PawnSnake.h"
#include "Camera/CameraComponent.h"
#include "Components/SceneComponent.h"
#include "Components/InputComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "Snake/Snake.h"

// Sets default values
APawnSnake::APawnSnake()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    auto Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
    RootComponent = Root;
    Camera->AttachToComponent(Root, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

}

// Called when the game starts or when spawned
void APawnSnake::BeginPlay()
{
    Super::BeginPlay();


    SpawnSnake();

    FRotator Rot(-90, 0, 0);
    Camera->SetRelativeRotation(Rot);
    Camera->SetProjectionMode(ECameraProjectionMode::Orthographic);
    Camera->SetOrthoWidth(CameraDistance);

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (PlayerController) {
        GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Get Player Controller"));
        ULocalPlayer* LocalPlayer = PlayerController->GetLocalPlayer();
        if (LocalPlayer) {
            GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Get Local Player"));
            UEnhancedInputLocalPlayerSubsystem* Subsystem = LocalPlayer->GetSubsystem<UEnhancedInputLocalPlayerSubsystem>();
            if (Subsystem)
            {
                GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Mapping subsystem"));
                Subsystem->AddMappingContext(InputMapping, 0);
            }
        }
    }
}

// Called every frame
void APawnSnake::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    if (IsValid(Snake))
    {
        Snake->Move();
    }

}

// Called to bind functionality to input
void APawnSnake::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Setup input component"));
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    UEnhancedInputComponent* Input = Cast<UEnhancedInputComponent>(PlayerInputComponent);
    Input->BindAction(InputAction, ETriggerEvent::Triggered, this, &APawnSnake::HandlerInputAction);

}

void APawnSnake::SpawnSnake() {
    if (IsValid(Snake)) {
        return;
    }


    FTransform Transform = FTransform();
    Transform.SetTranslation(SpawnOffset);
    Snake = GetWorld()->SpawnActor<ASnake>(SnakeClass, Transform);
    Snake->OnDeadDelegate.AddDynamic(this, &APawnSnake::HandleOnDead);

}

void APawnSnake::DestroySnake()
{
    if (Snake && IsValid(Snake))
    {
        Snake->DestroySnakeElements();
        Snake->Destroy();
        Snake = nullptr;
    }
}


void APawnSnake::HandlerInputAction(const FInputActionInstance& Instance)
{

    FVector AxisValue = Instance.GetValue().Get<FVector>();
    if (IsValid(Snake))
        Snake->SetNewDirection(AxisValue);
}

void APawnSnake::Dead_Implementation()
{
}

void APawnSnake::HandleOnDead()
{
    OnDeadDelegate.Broadcast();
}

